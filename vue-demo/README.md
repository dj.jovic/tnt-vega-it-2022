# Uputstvo za mlade istrazivace

Da bi ste pokrenuli aplikaciju dovoljno je da uradite
*npm install*
*npm run dev*

Ukoliko zelite da napravite aplikacija da bude *web component*, potrazite komentar **// Zameniti** i zatim izvrsite:
*npm run build*

Na kraju mozete uzeti *demo-vue.js* u folderu *dist* i koristiti ga bilo gde kao web komponentu pod nazivom *<demo-vue></demo-vue>*

## Stize uskrs!
Gledanje van *prozora* moze pokazati *easter egg*