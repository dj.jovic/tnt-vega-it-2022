import React, { FC, useContext, useEffect, useState } from 'react';
import { EventContext, Styled } from 'direflow-component';
import styles from './App.css';

interface IProps {
  componentTitle: string;
  sampleList: string[];
}

const useApp = () => {
  const [cards, setCards] = useState([
    {
      id: 0,
      title: `I am react element`,
    },
    {
      id: 1,
      title: `I am react element`,
    },
    {
      id: 2,
      title: `I am react element`,
    }
  ]);

  // Zameni me
  // const [allowedToStart, setAllowedToStart] = useState(false);
  const [allowedToStart, setAllowedToStart] = useState(true);

  // Zameni me
  // window.addEventListener('start-react', () => setAllowedToStart(true));
  window.dispatchEvent(new Event('va-ge'));
  
  const addItemInLoop = async () => {
    if (cards.length >= 8 && !!allowedToStart) {
      return;
    }

    setCards([
      ...cards.map(x => ({ id: x.id, title: `I am react element updated` })),
      {
        id: cards.length,
        title: `I am react element`
      }
    ]);
  }

  useEffect(() => {
    if(allowedToStart) {
      setTimeout(() => {
        addItemInLoop();
      }, 1000)
    }
  }, [cards, allowedToStart]);

  return {
    cards,
  }
}


const App: FC<IProps> = (props) => {
  const { cards } = useApp();

  return (
    <Styled styles={styles}>
    <div className="flex flex-row">
      {
        cards.map(card => (
          <div className="flex flex-col bordered padded" key={card.id}>
            <h2>{card.title}</h2>
            <img width="100px" height="100px" alt="react-logo" src={'./' + process.env.PUBLIC_URL + '/assets/logo.webp'} />
          </div >
        ))
      }

    </div >
    </Styled>
  );
};

App.defaultProps = {
  componentTitle: 'React Web',
  sampleList: [
    'Create with React',
    'Build as Web Component',
    'Use it anywhere!',
  ],
}

export default App;
