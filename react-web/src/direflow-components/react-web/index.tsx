import { DireflowComponent } from 'direflow-component';
import App from './App';

setTimeout(() => {
  console.clear();
  console.warn('Summoned i am, but with wrong name, call me with right one and i\'ll unlock a secret game!')
}, 3000)
export default DireflowComponent.create({
  component: App,
  configuration: {
    tagname: 'demo-react',
  },
  plugins: [
    {
      name: 'font-loader',
      options: {
        google: {
          families: ['Advent Pro', 'Noto Sans JP'],
        },
      },
    },
  ],
});
