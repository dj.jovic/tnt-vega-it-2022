/**
 * This is the entry file of the Direflow setup.
 *
 * You can add any additional functionality here.
 * For example, this is a good place to hook into your
 * Web Component once it's mounted on the DOM.
 *
 * !This file cannot be removed.
 * It can be left blank if not needed.
 */

import ReactWeb from './direflow-components/react-web';

ReactWeb.then((element) => {

  /**
   * Access DOM node when it's mounted
   */
  console.log('react-web is mounted on the DOM', element);

  window.addEventListener('ve-ga', () => console.warn(atob('Q2FvISBUYWpuYSBqZSBvdGtsanVjYW5hIQpWZWdhIGplIGludGVueml2bm8gdSBwb3RyYXpuamkgemEgbGp1ZGUgc2EgaXN0cmF6aXZhY2tpbSBkdWhvbSAtIHBvc2Fsamkgc3ZvaiBDViBuYSBwZW9wbGVAdmVnYWl0LnJzIGkgcHJvc2xlZGkgdHZvaiB0YWpuaSBrb2QgIlBhemxqaXZpIHBveml2aSI=')));
});
