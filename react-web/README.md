# Uputstvo za mlade istrazivace

Da bi ste pokrenuli aplikaciju dovoljno je da uradite
*npm install*
*npm run start*

Ukoliko zelite da napravite aplikacija da bude *web component*, potrazite komentar **// Zameniti** i zatim izvrsite:
*npm run build:single*

Na kraju mozete uzeti *demo-react.js* u folderu *build* i koristiti ga bilo gde kao web komponentu pod nazivom *<demo-react></demo-react>*
Da bi ste pokrenuli logiku za dodavenje elemenata potrebno je *pozvati* Event "start-react"

## Stize uskrs!
Gledanje van *prozora* moze pokazati *easter egg*