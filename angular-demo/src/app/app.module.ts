import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

// Zameniti
// @NgModule({
//   declarations: [AppComponent],
//   imports: [BrowserModule],
//   providers: [],
//   // bootstrap: [AppComponent]
// })
// export class AppModule {
//   constructor(private injector: Injector) {}

//   // Sami radimo bootstrapovanje - i stavljamo u novu web komponentu
//   ngDoBootstrap() {
//     const customElem = createCustomElement(AppComponent, { injector: this.injector });
//     customElements.define('demo-angular', customElem);
//   }
// }

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
