import { ICard } from './../models/card.model';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { interval, takeWhile, tap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent implements OnInit {
  title = 'angular-demo';
  cards: ICard[] = [];

  ngOnInit(): void {
    for (let i = 0; i < 3; i++) {
      this.cards.push({
        id: i,
        title: `I am angular element`,
      });
    }

    // Zameniti
    // window.addEventListener('angular-start', () => {
    //   this.startLoop();
    // });

    // A ovo zakokemntarisati
    this.startLoop();
  }

  startLoop() {
    let loopCount = 0;
    interval(1000)
      .pipe(
        takeWhile((_) => loopCount < 5),
        tap(_ => {
          this.cards.forEach(x => x.title = `I am angular element updated`)
        })
      )
      .subscribe((x) => {
        this.cards.push({
          id: this.cards.length,
          title: `I am angular element`,
        });
        loopCount++;
      });
  }

  callWindowFn(fn: string) {
    (window as any)[fn]();
  }
}
