# Uputstvo za mlade istrazivace

Da bi ste pokrenuli aplikaciju dovoljno je da uradite
*npm install*
*npm run start*

Ukoliko zelite da napravite aplikacija da bude *web component*, potrazite komentar **// Zameniti** i zatim izvrsite:
*npm run build:single*
*npm run package*

Na kraju mozete uzeti *demo-angular.js* u folderu *dist* i koristiti ga bilo gde kao web komponentu pod nazivom *<demo-angular></demo-angular>*
Da bi ste pokrenuli logiku za dodavenje elemenata potrebno je *pozvati* Event "angular-start"

## Stize uskrs!
Gledanje van *prozora* moze pokazati *easter egg*